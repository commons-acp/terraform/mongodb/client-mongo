
resource "mongodb_db_user" "user" {
  depends_on = [mongodb_db_role.create_db_role]
  auth_database = "DB-${var.mongo_db_user}"
  name = var.mongo_db_user
  password = var.mongo_db_pwd
  role {
    role = "role-${var.mongo_db_user}"
    db =   "DB-${var.mongo_db_user}"
  }

}
