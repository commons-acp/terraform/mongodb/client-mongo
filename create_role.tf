
resource "mongodb_db_role" "create_db_role" {
  name = "role-${var.mongo_db_user}"
  database = "DB-${var.mongo_db_user}"
  privilege {
    db = "DB-${var.mongo_db_user}"
    collection = ""
    actions =  [ "collStats", "dbHash", "dbStats", "find", "killCursors", "listIndexes", "listCollections", "convertToCapped", "createCollection", "createIndex", "dropIndex", "insert", "remove", "renameCollectionSameDB", "update","dropCollection"]
  }


}
