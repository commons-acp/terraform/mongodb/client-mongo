terraform {
  required_providers {
    mongodb = {
      source = "Kaginari/mongodb"
      version = "0.0.4"
    }
  }
}
